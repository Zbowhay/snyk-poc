import { map } from "lodash";

// just make a map
const data = ['jimmy,boston', 'suzy,demi', 'j,cole'];
interface Person {
    firstName: string;
    lastName: string;
}
const people = map(data, (item) => {
    const [firstName,lastName] = item.split(',');
    return {
        firstName,
        lastName
    } as Person;
});

console.log(people);