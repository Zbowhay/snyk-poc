import express from 'express';
const app = express();
const host = '0.0.0.0';
const port = 10900;

app.get('/', (req, res) => {
    res.send('Howdy from app1');
});

app.listen(port, host, () => { console.log(`app1 listening on ${host}:${port}...`)});